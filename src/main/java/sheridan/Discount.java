/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author jayvy
 */
public abstract class Discount {
    double discountpercentage;
    
    Discount(double discountpercentage){
        this.discountpercentage=discountpercentage;
        
    }
    public void setDiscountPercentage(double discountpercentage){
     this.discountpercentage=discountpercentage;
        
    }
    public abstract double calculateDiscount(double amount);

}
