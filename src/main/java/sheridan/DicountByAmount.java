/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author jayvy
 */
public class DicountByAmount extends Discount {
    private double discountDollarAmount;
    public DicountByAmount(double discountpercentage,double discountDollarAmount){
        super(discountpercentage);
        this.discountDollarAmount=discountDollarAmount;
        
    }
    public double calculateDiscount(double amount){
        
        return discountDollarAmount;
        
    }

  
}
